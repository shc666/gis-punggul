<style>
    #chartpie1 {
        width: 100%;
        height: 500px;
    }
    #chartpie2 {
        width: 100%;
        height: 500px;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Status Tanah</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">
                                <strong>Data Berdasarkan Keseluruhan</strong>
                            </p>
                            <div class="chart-responsive">
                                <div id="chartpie1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Banjar</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">
                                <strong>Data Berdasarkan Keseluruhan</strong>
                            </p>
                            <div class="chart-responsive">
                                <div id="chartpie2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
    /*
        Pie Chart - Status
    */
    am4core.ready(function() {
        am4core.useTheme(am4themes_material);
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chartpie1", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0;

        chart.data = [
            @foreach($status as $data)
                {
                    status: "{{ $data->status_tanah }}",
                    jumlah: {{ $data->jumlah_status }}
                },
            @endforeach
        ];

        chart.innerRadius = am4core.percent(50);
        chart.depth = 50;

        chart.legend = new am4charts.Legend();

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "jumlah";
        series.dataFields.depthValue = "jumlah";
        series.dataFields.category = "status";
        series.slices.template.cornerRadius = 5;
        series.colors.step = 3;
    });

    /*
        Pie Chart - Banjar
    */
    am4core.ready(function() {
        am4core.useTheme(am4themes_material);
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chartpie2", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0;

        chart.data = [
            @foreach($banjar as $data)
                {
                    banjar: "{{ $data->banjar }}",
                    jumlah: {{ $data->jumlah_banjar }}
                },
            @endforeach
        ];

        chart.innerRadius = am4core.percent(40);
        chart.depth = 120;

        chart.legend = new am4charts.Legend();

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "jumlah";
        series.dataFields.depthValue = "jumlah";
        series.dataFields.category = "banjar";
        series.slices.template.cornerRadius = 5;
        series.colors.step = 3;
    });
</script>