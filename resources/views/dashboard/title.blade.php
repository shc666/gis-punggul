<style>
    .title {
        font-size: 50px;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;
        display: block;
        text-align: center;
        margin: 20px 0 10px 0px;
    }
</style>

<div class="title">
    GIS Tanah Desa Punggul
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{ $total_data }}</h3>
                <p>Jumlah Data Tanah</p>
            </div>
            <div class="icon">
                <i class="fa fa-list-alt"></i>
            </div>
            <a href="{{ admin_url('data-tanah') }}" class="small-box-footer">
                Lihat Data
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>