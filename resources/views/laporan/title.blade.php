<div class="row">
    <div class="col-md-12">
        @if(Session::has('flash_message_error'))
        <div class="alert alert-danger" id="danger-alert">
            <strong>
                <span>
                    <i class="fa fa-ban"></i>
                </span>
            </strong>
            {{ Session::get('flash_message_error') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
        @endif
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Filter Laporan Berdasarkan Banjar</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="display: block;">
                <form id="custom_form" action="{{ route('admin.post.laporan') }}" method="post" accept-charset="UTF-8" class="form-horizontal" pjax-container>
                    {{ csrf_field() }}
                    <div class="box-body form-horizontal">
                        <div class="col-lg-3">
                            <label>Kategori Laporan</label>
                            <select class="form-control" name="filter" aria-hidden="true" id="filter">
                                <option></option>
                                <option value="1">Banjar</option>
                                <option value="2">Status</option>
                                <option value="3">Rt/Rw</option>
                                <option value="4">Nilai ZNT</option>
                                <option value="5">NJOP</option>
                            </select>
                        </div>
                        <div class="col-lg-3" id="status">
                            <label>Status</label>
                            <select class="form-control" name="status" id="filter_status" aria-hidden="true">
                                <option></option>
                                <option value="all">Semua data</option>
                                @foreach ($status as $data)
                                <option value="{{ $data }}">{{ $data }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3" id="rtrw">
                            <label>Rt/Rw</label>
                            <select class="form-control" name="rtrw" id="filter_rtrw" aria-hidden="true">
                                <option></option>
                                <option value="all">Semua data</option>
                                @foreach ($rtrw as $data)
                                <option value="{{ $data }}">{{ $data }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3" id="znt">
                            <label>Nilai ZNT</label>
                            <select class="form-control" name="znt" id="filter_znt" aria-hidden="true">
                                <option></option>
                                <option value="all">Semua data</option>
                                @foreach ($znt as $data)
                                <option value="{{ $data }}">{{ $data }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3" id="banjar">
                            <label>Banjar</label>
                            <select class="form-control" name="banjar" id="filter_banjar" aria-hidden="true">
                                <option></option>
                                <option value="all">Semua data</option>
                                @foreach ($banjar as $data)
                                <option value="{{ $data }}">{{ $data }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3" id="submit">
                            <button type="submit" class="btn btn-primary" style="margin-top: 25px">
                                <i class="fa fa-search"></i>
                                Filter
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("#filter").select2({"allowClear":true,"placeholder":{"id":"","text":"Silahkan Pilih Tipe Laporan"}});
        $("#filter_banjar").select2({"allowClear":true,"placeholder":{"id":"","text":"Silahkan Pilih"}});
        $("#filter_status").select2({"allowClear":true,"placeholder":{"id":"","text":"Silahkan Pilih"}});
        $("#filter_rtrw").select2({"allowClear":true,"placeholder":{"id":"","text":"Silahkan Pilih"}});
        $("#filter_znt").select2({"allowClear":true,"placeholder":{"id":"","text":"Silahkan Pilih"}});
    });

    $(document).ready(function() {
        $("#danger-alert").fadeTo(3000, 500).slideUp(500, function() {
            $("#danger-alert").slideUp(500);
        });
        
        $('#banjar, #status, #rtrw, #znt, #submit').hide();

        $('#filter').change(function() {
            if($(this).val() == '') {
                $('#banjar, #status, #rtrw, #znt, #submit').hide();
            }
            else if($(this).val() == 1 || $(this).val() == 5) {
                $('#banjar, #submit').show();
                $('#status, #rtrw, #znt').hide();
            }
            else if($(this).val() == 2) {
                $('#status, #banjar,  #submit').show();
                $('#rtrw, #znt').hide();
            }
            else if($(this).val() == 3) {
                $('#rtrw, #banjar, #submit').show();
                $('#status, #znt').hide();
            }
            else if($(this).val() == 4) {
                $('#znt, #banjar, #submit').show();
                $('#status, #rtrw').hide();
            }

            $('#filter select, #status select, #rtrw select, #znt select, #banjar select').val('');
        });
    })
</script>