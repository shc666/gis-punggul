<html>
    <head>
        <style type="text/css">
            .body {
                font-size: 12pt;
                font-family: "Times New Roman", Times, serif;
                border-spacing: 0;
            }
            div {
                text-align: center; 
                padding-bottom: 2px;
            }
            h2 {
                font-size: 13pt;
                text-align: center;
                margin: 0px 5px 2px -80px !important;
            }
            h3 {
                font-size: 13pt;
                text-align: center;
                margin: 15px 5px 2px -80px !important;
                font-weight: normal;
            }
            h4 {
                font-size: 12pt;
                text-align: center;
                margin: 0px 5px 2px -80px !important;
                font-weight: normal;
            }
            h5 {
                font-size: 10pt;
                text-align: center;
                margin: 0px 5px 2px -80px !important;
                font-weight: normal;
            }
            table, th, td {
                align: justify;
            }
            td {
                vertical-align: top;
                align: justify;
                text-align: justify;
            }
            .custom-border>thead>tr>th,
            .custom-border>tbody>tr>th,
            .custom-border>tfoot>tr>th,
            .custom-border>thead>tr>td,
            .custom-border>tbody>tr>td,
            .custom-border>tfoot>tr>td {
                border: 1px solid #444444 !important;
            }
            hr {
                display: block;
                height: 2px;
                border: 0;
                border-top: 7px double #000;
                margin: 0px 0px 0px 0px;
                padding: 0; 
            }
            hr.top {
                margin-top: 5px;
                margin-bottom: 0px;
                border-top: 1px solid #000;
        
            }
            hr.sub-header {
                margin: 1rem;
            }
            #table {
                border-collapse:collapse !important;
                border-bottom: 1px solid #000;
                page-break-before: auto;
            }
            .header {
                font-size: 12pt;
                margin-top: 20px;
                text-align: center;
            }
            .text {
                text-indent: 0.3in;
                text-align: justify;
                margin-bottom: 0px!important;
            }
            
            /* Custom Table  */
            .tg {
                border-collapse: collapse;
                border-color:#ccc;
                border-spacing:0;
            }
            .tg td {
                background-color:#fff;
                border-color:#ccc;
                border-style:solid;
                border-width:0px;
                color:#333;
                font-family:Arial, sans-serif;
                font-size:14px;
                overflow:hidden;
                padding:10px 5px;
                word-break:normal;
            }
            .tg th {
                background-color:#f0f0f0;
                border-color:#ccc;
                border-style:solid;
                border-width:0px;
                color:#333;
                font-family:Arial, sans-serif;
                font-size:14px;
                font-weight:normal;
                overflow:hidden;
                padding:10px 5px;
                word-break:normal;
            }
            .tg .tg-7btt {
                border-color:inherit;
                font-weight:bold;
                text-align:center;
                vertical-align:top;
                text-align: left;
            }
            .tg .tg-0pky {
                border-color:inherit;
                text-align:center;
                vertical-align:top;
                text-align: left;
            }
        </style>
    </head>
    <body>
        <page backtop="15mm" backbottom="25mm" backleft="20mm" backright="15mm" class="body">
            <table style="border-bottom:0;" border="0" width="100%" id="table">
                <tr>
                    <td style="text-align: center">
                        <img id="logo" src="{{ public_path('/frontend-assets/map-site/img/logo-badung.png')}}" width="150" height="150" alt="Logo Badung">
                    </td>
                </tr>
            </table>
            <table style="border-bottom:0;" border="0" width="100%" id="table">
                <tr>
                    <td align="center" width="470">
                        <h3>Desa Punggul - Kabupaten Badung</h3>
                    </td>
                </tr>
            </table>
            <hr class="sub-header">
            <table style="border-bottom:0;" border="0" width="100%" id="table">
                <tr>
                    <td align="center" width="470">
                        @isset($filter_banjar)
                        <h3 style="font-size: 24px;">Data NJOP {{ $filter_banjar }} di Desa Punggul</h3>
                        @endisset
                    </td>
                </tr>
            </table>
            <br>
            <table class="tg" width="100%" style="undefined;table-layout: fixed; width: 100%">
                <colgroup>
                    <col style="width: 303px">
                    <col style="width: 301px">
                </colgroup>
                <thead>
                    <tr>
                        <th class="tg-7btt">No.</th>
                        <th class="tg-7btt">Nama</th>
                        <th class="tg-7btt">NIK</th>
                        <th class="tg-7btt">Nama Banjar</th>
                        <th class="tg-7btt">NJOP</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($reports as $report)
                    <tr>
                        <td class="tg-0pky">
                            <p class="text">{{ $no++ }}</p>
                        </td>
                        <td class="tg-0pky">
                            <p class="text">{{ $report->nama_wp }}</p>
                        </td>
                        <td class="tg-0pky">
                            <p class="text">{{ $report->nik }}</p>
                        </td>
                        <td class="tg-0pky">
                            <p class="text">{{ $report->banjar }}</p>
                        </td>
                        <td class="tg-0pky">
                            <p class="text">Rp. {{ format_uang($report->njop) }}</p>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <br>
                <br>
            <table class="tg" width="100%" style="undefined;table-layout: fixed; width: 100%">
                <colgroup>
                    <col style="width: 303px">
                    <col style="width: 301px">
                </colgroup>
                <thead>
                    <tr>
                        <th class="tg-7btt" style="text-align: center;">
                            @isset($filter_banjar)
                            <p class="text">Total Keseluruhan Data NJOP {{ $filter_banjar }} di Desa Punggul</p>
                            @endisset
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tg-0pky" style="text-align: center;">
                            <p class="text"><b>Rp. {{ format_uang($report_count['jumlah_total_njop']) }}</b></p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </page>
    </body>
</html>