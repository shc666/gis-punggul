<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Total data sebanyak: &nbsp;<strong>{{ $report_count }}</strong>
                    </h3>
                    @isset($jumlah_znt)
                    <h3 class="box-title" style="margin-left: 50px">
                        Total jumlah ZNT: &nbsp;<strong>{{ $jumlah_znt['jumlah_total_znt'] }}</strong>
                    </h3>
                    @endisset
                    @isset($jumlah_njop)
                    <h3 class="box-title" style="margin-left: 50px">
                        Total jumlah NJOP: &nbsp;<strong>Rp. {{ format_uang($jumlah_njop['jumlah_total_njop']) }}</strong>
                    </h3>
                    @endisset
                    <form action="{{ route('admin.cetak.laporan') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="filter" value="{{ $filter }}">
                        <input type="hidden" name="banjar" value="{{ $filter_banjar }}">
                        <input type="hidden" name="status" value="{{ $filter_status }}">
                        <input type="hidden" name="rtrw" value="{{ $filter_rtrw }}">
                        <input type="hidden" name="znt" value="{{ $filter_znt }}">
                        <button type="submit" class="btn bg-navy pull-right btn-sm" data-toggle="tooltip" title="Cetak Laporan">
                            <i class="fa fa-file-pdf-o"></i> Cetak Laporan
                        </button>
                    </form>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-bordered table-striped" id="filter-table-wrapper">
                                @if($filter == 1 || $filter == 2 || $filter == 3)
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th width="20%">Nama</th>
                                        <th width="20%">NIK</th>
                                        <th width="10%">Banjar</th>
                                        <th width="30%">RT / RW</th>
                                        <th width="20%">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach($reports as $report)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $report->nama_wp }}</td>
                                        <td>{{ $report->nik }}</td>
                                        <td>{{ $report->banjar}}</td>
                                        <td>{{ $report->rtrw}}</td>
                                        <td>{{ $report->status}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                @elseif($filter == 4)
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th width="20%">Nama</th>
                                        <th width="15%">NIK</th>
                                        <th width="10%">Banjar</th>
                                        <th width="30%">RT / RW</th>
                                        <th width="10%">Nilai ZNT</th>
                                        <th width="30%">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach($reports as $report)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $report->nama_wp }}</td>
                                        <td>{{ $report->nik }}</td>
                                        <td>{{ $report->banjar}}</td>
                                        <td>{{ $report->rtrw}}</td>
                                        <td>{{ $report->nilai_znt}}</td>
                                        <td>{{ $report->status}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                @elseif($filter == 5)
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th width="20%">Nama</th>
                                        <th width="15%">NIK</th>
                                        <th width="10%">Banjar</th>
                                        <th width="30%">RT / RW</th>
                                        <th width="10%">NJOP</th>
                                        <th width="30%">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach($reports as $report)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $report->nama_wp }}</td>
                                        <td>{{ $report->nik }}</td>
                                        <td>{{ $report->banjar}}</td>
                                        <td>{{ $report->rtrw}}</td>
                                        <td>{{ $report->njop}}</td>
                                        <td>{{ $report->status}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('#filter-table-wrapper').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : false,
            'info'        : true,
            'autoWidth'   : true,
            'language'    : {
                "url": "http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"
            }
        });
    });
</script>