<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        {{-- Meta --}}
        <!-- Meta Tags -->
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
        <!-- description -->
        <meta name="description" content="map-site is a highly creative, modern, visually stunning and Bootstrap responsive multipurpose studio.">
        <!-- keywords -->
        <meta name="keywords" content="map-site, immortalsolutions, gis, punggul, badung">
        <!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- CSS --}}
        <!-- Page Title -->
        <title>{{ config('app.name')}}</title>
        <!-- Favicon -->
        <link href="{{ url('frontend-assets/map-site/img/logos/favicon.ico') }}" rel="shortcut icon">
        <!-- Bundle -->
        <link href="{{ url('frontend-assets/vendor/css/bundle.min.css') }}" rel="stylesheet">
        <!-- Plugin Css -->
        <link href="{{ url('frontend-assets/map-site/css/line-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ url('frontend-assets/vendor/css/jquery.fancybox.min.css') }}" rel="stylesheet">
        <link href="{{ url('frontend-assets/vendor/css/owl.carousel.min.css') }}" rel="stylesheet">
        <link href="{{ url('frontend-assets/vendor/css/cubeportfolio.min.css') }}" rel="stylesheet">
        <!-- Style Sheet -->
        <link href="{{ url('frontend-assets/map-site/css/blog.css') }}" rel="stylesheet">
        <link href="{{ url('frontend-assets/map-site/css/style.css') }}" rel="stylesheet">
        <!-- Custom Style CSS File -->
        <link rel="stylesheet" href="{{ url('frontend-assets/map-site/css/custom.css') }}">
        <!-- Leafet Map CSS-->
        <link rel="stylesheet" href="{{ url('frontend-assets/map-site/css/leaflet.css') }}">
    </head>

    <body data-spy="scroll" data-target=".navbar" data-offset="90">
        <!-- Preloader -->
        <!--START PAGE LOADER-->
        <div id="loader">
            <div class="loader center-block">
                <div class="inner one"></div>
                <div class="inner two"></div>
                <div class="inner three"></div>
            </div>
        </div>
        <!--END PAGE LOADER-->
        <!-- Preloader End -->
        
        <div id="app"></div>
        {{-- Scripts --}}
        <!-- JavaScript -->
        <script src="{{ url('frontend-assets/vendor/js/bundle.min.js') }}"></script>
        <script src="{{ url('frontend-assets/vendor/js/parallaxie.min.js') }}"></script>
        <!-- custom script-->
        <script src="{{ url('frontend-assets/map-site/js/script.min.js') }}"></script>
        <script src="{{ asset('/js/app.js')}}"></script>
    </body>
</html>