require('./bootstrap');
window.Vue = require('vue');

/** 
 * Main Structure Layouts
*/
import IndexMap from './components/Index.vue'


/**
 * Vue Router Callback And Global Plugins
*/
import Vue from 'vue'
import { LControl, LControlLayers, LMap, LTileLayer, LMarker, LGeoJson } from 'vue2-leaflet'
import VueProgressBar from 'vue-progressbar'
// import 'leaflet/dist/leaflet.css'

Vue.component('l-map', LMap)
Vue.component('l-tile-layer', LTileLayer)
Vue.component('l-marker', LMarker)
Vue.component('l-control', LControl)
Vue.component('l-control-layers', LControlLayers)
Vue.component('l-geo-json', LGeoJson)
Vue.use(VueProgressBar, {
    color: '#28c205',
    failedColor: 'red',
    height: '3px',
    thickness: '5px',
    transition: {
      speed: '0.5s',
      opacity: '0.6s',
      termination: 300
    },
})

/**
 * Vue Render Init
 */
const vm = new Vue({
    el: '#app',
    render: h => h(IndexMap)
});