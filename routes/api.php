<?php

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.', 'namespace' => 'API'], function () {
    /*
     * Outlets Endpoints
     */
    Route::get('data-outlets', 'SpatialController@index')->name('outlets.index');
    Route::post('filter-outlets', 'SpatialController@postFilter')->name('outlets.filter');
});