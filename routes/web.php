<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

Route::view('/', 'app')->name('index');
Route::get('/update-app', function()
{
    Artisan::call('dump-autoload');
});
Route::get('/storage-link', function () {
    Artisan::call('storage:link');
});