<?php

namespace App\Admin\Controllers;

use App\TTanahPunggul;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TanahDesaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Manage Data Tanah Desa Punggul';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new TTanahPunggul());

        /* Tools */
        $grid->disableCreateButton();
        $grid->disableColumnSelector();
        $grid->disableRowSelector();
        $grid->disableExport();

        /* Filter */
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('nama_wp', 'Filter Berdasarkan Nama');
            $filter->equal('nik', 'Filter Berdasarkan NIK')->integer();
            $filter->equal('status', 'Filter Berdasarkan Status')->select(TTanahPunggul::get()->pluck('status', 'status'));
            $filter->equal('banjar', 'Filter Berdasarkan Banjar')->select(TTanahPunggul::get()->pluck('banjar', 'banjar'));
        });

        /* Actions */
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        /* Dataset */
        // $grid->column('SHAPE', __('SHAPE'));
        // $grid->column('cartodb_id', __('Cartodb id'));
        // $grid->column('kabupaten', __('Kabupaten'));
        // $grid->column('kecamatan', __('Kecamatan'));
        // $grid->column('desa', __('Desa'));
        // $grid->column('alamat_wp', __('Alamat wp'));
        // $grid->column('alamat_op', __('Alamat op'));
        // $grid->column('nib', __('Nib'));
        // $grid->column('su', __('Su'));
        // $grid->column('luas_shat', __('Luas shat'));
        $grid->column('OGR_FID', 'ID');
        $grid->column('banjar', __('Banjar'));
        $grid->column('nama_wp', __('Nama wp'));
        $grid->column('nik', __('Nik'));
        $grid->column('status', __('Status'));
        $grid->column('nop', __('Nop'));
        $grid->column('luas_sppt', __('Luas sppt'));
        $grid->column('nilai_znt', __('Nilai znt'));
        $grid->column('rtrw', __('Rtrw'));
        $grid->column('njop', __('Njop'));
        $grid->column('bpajb', __('Bpajb'));
        $grid->paginate(50);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(TTanahPunggul::findOrFail($id));

        $show->field('OGR_FID', __('OGR FID'));
        $show->field('SHAPE', __('SHAPE'));
        $show->field('cartodb_id', __('Cartodb id'));
        $show->field('kabupaten', __('Kabupaten'));
        $show->field('kecamatan', __('Kecamatan'));
        $show->field('desa', __('Desa'));
        $show->field('banjar', __('Banjar'));
        $show->field('nib', __('Nib'));
        $show->field('su', __('Su'));
        $show->field('luas_shat', __('Luas shat'));
        $show->field('nik', __('Nik'));
        $show->field('status', __('Status'));
        $show->field('nop', __('Nop'));
        $show->field('nama_wp', __('Nama wp'));
        $show->field('alamat_wp', __('Alamat wp'));
        $show->field('alamat_op', __('Alamat op'));
        $show->field('luas_sppt', __('Luas sppt'));
        $show->field('nilai_znt', __('Nilai znt'));
        $show->field('rtrw', __('Rtrw'));
        $show->field('njop', __('Njop'));
        $show->field('bpajb', __('Bpajb'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new TTanahPunggul());

        /* Tools */
        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
        });

        // $form->text('SHAPE', __('SHAPE'));
        // $form->decimal('cartodb_id', __('Cartodb id'));
        $form->text('kabupaten', __('Kabupaten'))->rules("required");
        $form->text('kecamatan', __('Kecamatan'))->rules("required");
        $form->text('desa', __('Desa'))->rules("required");
        $form->select('banjar', __('Banjar'))->options(TTanahPunggul::select('banjar')->groupBy('banjar')->get()->pluck('banjar', 'banjar'))->rules("required");
        $form->text('nib', __('Nib'))->rules("required");
        $form->text('su', __('Su'))->rules("required");
        $form->text('luas_shat', __('Luas shat'))->rules("required");
        $form->number('nik', __('Nik'))->rules("required");
        $form->select('status',  __('Status'))->options(TTanahPunggul::select('status')->groupBy('status')->get()->pluck('status', 'status'))->rules("required");
        $form->number('nop', __('Nop'))->rules("required");
        $form->text('nama_wp', __('Nama wp'))->rules("required");
        $form->text('alamat_wp', __('Alamat wp'))->rules("required");
        $form->text('alamat_op', __('Alamat op'))->rules("required");
        $form->text('luas_sppt', __('Luas sppt'))->rules("required");
        $form->number('nilai_znt', __('Nilai znt'))->rules("required");
        $form->select('rtrw', __('Rtrw'))->options(TTanahPunggul::select('rtrw')->groupBy('rtrw')->get()->pluck('rtrw', 'rtrw'))->rules("required");
        $form->number('njop', __('Njop'))->rules("required");
        $form->text('bpajb', __('Bpajb'))->rules("required");

        return $form;
    }
}