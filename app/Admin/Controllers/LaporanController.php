<?php

namespace App\Admin\Controllers;

use App\TTanahPunggul;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\Input;
use DB;
use PDF;

class LaporanController extends Controller
{
    use HasResourceActions;

    /**
     * Main Index Laporan
     *
     * @param Content $content
     * @return Content
     */
    public function index()
    {
        return Admin::content(function(Content $content) {
            $content->header('Manage Laporan Data Tanah Desa Punggul');
            $content->description('List');

            $banjar = TTanahPunggul::get()->pluck('banjar', 'banjar');
            $rtrw = TTanahPunggul::select('rtrw')->groupBy('rtrw')->get()->pluck('rtrw', 'rtrw');
            $status = TTanahPunggul::select('status')->groupBy('status')->get()->pluck('status', 'status');
            $znt = TTanahPunggul::select('nilai_znt')->groupBy('nilai_znt')->get()->pluck('nilai_znt', 'nilai_znt');

            $content->row(view('laporan.title')->with(compact('banjar', 'rtrw', 'status', 'znt')));

            /* Filter Laporan */
            $filter = Input::get('filter', null);
            $filter_banjar = Input::get('banjar', null);
            $filter_status = Input::get('status', null);
            $filter_rtrw = Input::get('rtrw', null);
            $filter_znt = Input::get('znt', null);

            if(isset($filter))
            {
                if($filter == 1 || $filter == 2 || $filter == 3)
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $content->body(view('laporan.body')->with(compact('filter', 'filter_banjar', 'filter_status', 'filter_rtrw', 'filter_znt', 'reports', 'report_count')));
                }
                elseif($filter == 4)
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);
                    $jumlah_znt = TTanahPunggul::where('banjar', $filter_banjar)->select(DB::raw('SUM(nilai_znt)as jumlah_total_znt'))->first();

                    $content->body(view('laporan.body')->with(compact('filter', 'filter_banjar', 'filter_status', 'filter_rtrw', 'filter_znt', 'jumlah_znt', 'reports', 'report_count')));
                }
                elseif($filter == 5)
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);
                    if($filter_banjar == 'all')
                    {
                        $jumlah_njop = TTanahPunggul::select(DB::raw('SUM(njop)as jumlah_total_njop'))->first();
                    }
                    else
                    {
                        $jumlah_njop = TTanahPunggul::where('banjar', $filter_banjar)->select(DB::raw('SUM(njop)as jumlah_total_njop'))->first();
                    }

                    $content->body(view('laporan.body')->with(compact('filter', 'filter_banjar', 'filter_status', 'filter_rtrw', 'filter_znt', 'jumlah_njop', 'reports', 'report_count')));
                }
            }
        });
    }

    /**
     * PDF filtering
     * 
     */
    public function cetakPdf()
    {
        $filter = Input::get('filter', null);
        $filter_banjar = Input::get('banjar', null);
        $filter_status = Input::get('status', null);
        $filter_rtrw = Input::get('rtrw', null);
        $filter_znt = Input::get('znt', null);

        if(isset($filter))
        {
            if($filter == 1)
            {
                if($filter_banjar == 'all')
                {
                    $banjar_kelodan   = TTanahPunggul::select('banjar', DB::raw('COUNT(banjar) as br_kelodan'))->where('banjar', 'BR KELODAN')->groupBy('banjar')->first();
                    $banjar_padang    = TTanahPunggul::select('banjar', DB::raw('COUNT(banjar) as br_padang'))->where('banjar', 'BR PADANG')->groupBy('banjar')->first();
                    $banjar_teguan    = TTanahPunggul::select('banjar', DB::raw('COUNT(banjar) as br_teguan'))->where('banjar', 'BR TEGUAN')->groupBy('banjar')->first();
                    $banjar_tengah    = TTanahPunggul::select('banjar', DB::raw('COUNT(banjar) as br_tengah'))->where('banjar', 'BR TENGAH')->groupBy('banjar')->first();
                    $banjar_trinadi   = TTanahPunggul::select('banjar', DB::raw('COUNT(banjar) as br_trinadi'))->where('banjar', 'BR TRINADI')->groupBy('banjar')->first();
                    $banjar_bengawan  = TTanahPunggul::select('banjar', DB::raw('COUNT(banjar) as br_bengawan'))->where('banjar', 'BR. BENGAWAN')->groupBy('banjar')->first();
                    $banjar_lainnya   = TTanahPunggul::select('banjar', DB::raw('COUNT(banjar) as br_lainnya'))->where('banjar', 'DILUAR BATAS DESA')->groupBy('banjar')->first();

                    $pdf = PDF::loadView('laporan.print_banjar_all', compact('banjar_kelodan', 'banjar_padang', 'banjar_teguan', 'banjar_tengah', 'banjar_trinadi', 'banjar_bengawan', 'banjar_lainnya'), [], [
                        'title'             => 'Laporan Data Banjar Desa Punggul',
                        'watermark'         => 'D A T A - B A N J A R',
                        'orientation'       => 'L',
                        'show_watermark'    => true
                    ]);
        
                    return $pdf->stream('Laporan-Data-Banjar-Desa-Punggul.pdf');
                }
                else
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_banjar', compact('filter_banjar', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data '.$filter_banjar.' Desa Punggul',
                                'watermark'         => 'D A T A - B A N J A R',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-'.$filter_banjar.'-Desa-Punggul.pdf');
                }
            }
            elseif($filter == 2)
            {
                if($filter_status == 'all' && $filter_banjar == 'all')
                {
                    $banjar_kelodan   = TTanahPunggul::select('banjar', 'status', DB::raw('COUNT(status) as br_kelodan'))->where('banjar', 'BR KELODAN')->groupBy('status', 'banjar')->get();
                    $banjar_padang    = TTanahPunggul::select('banjar', 'status', DB::raw('COUNT(status) as br_padang'))->where('banjar', 'BR PADANG')->groupBy('status', 'banjar')->get();
                    $banjar_teguan    = TTanahPunggul::select('banjar', 'status', DB::raw('COUNT(status) as br_teguan'))->where('banjar', 'BR TEGUAN')->groupBy('status', 'banjar')->get();
                    $banjar_tengah    = TTanahPunggul::select('banjar', 'status', DB::raw('COUNT(status) as br_tengah'))->where('banjar', 'BR TENGAH')->groupBy('status', 'banjar')->get();
                    $banjar_trinadi   = TTanahPunggul::select('banjar', 'status', DB::raw('COUNT(status) as br_trinadi'))->where('banjar', 'BR TRINADI')->groupBy('status', 'banjar')->get();
                    $banjar_bengawan  = TTanahPunggul::select('banjar', 'status', DB::raw('COUNT(status) as br_bengawan'))->where('banjar', 'BR. BENGAWAN')->groupBy('status', 'banjar')->get();
                    $banjar_lainnya   = TTanahPunggul::select('banjar', 'status', DB::raw('COUNT(status) as br_lainnya'))->where('banjar', 'DILUAR BATAS DESA')->groupBy('status', 'banjar')->get();

                    $count_kelodan  = DB::select('select sum(jumlah_status) as count_kelodan from (select count(status) as jumlah_status from t_tanah_desa_punggul where banjar = "BR KELODAN") as a');
                    $count_padang   = DB::select('select sum(jumlah_status) as count_padang from (select count(status) as jumlah_status from t_tanah_desa_punggul where banjar = "BR PADANG") as b');
                    $count_teguan   = DB::select('select sum(jumlah_status) as count_teguan from (select count(status) as jumlah_status from t_tanah_desa_punggul where banjar = "BR TEGUAN") as c');
                    $count_tengah   = DB::select('select sum(jumlah_status) as count_tengah from (select count(status) as jumlah_status from t_tanah_desa_punggul where banjar = "BR TENGAH") as d');
                    $count_trinadi  = DB::select('select sum(jumlah_status) as count_trinadi from (select count(status) as jumlah_status from t_tanah_desa_punggul where banjar = "BR TRINADI") as e');
                    $count_bengawan = DB::select('select sum(jumlah_status) as count_bengawan from (select count(status) as jumlah_status from t_tanah_desa_punggul where banjar = "BR. BENGAWAN") as f');
                    $count_lainnya  = DB::select('select sum(jumlah_status) as count_lainnya from (select count(status) as jumlah_status from t_tanah_desa_punggul where banjar = "DILUAR BATAS DESA") as g');
                    $count_all      = DB::select('select sum(jumlah_status) as count_all from (select count(status) as jumlah_status from t_tanah_desa_punggul) as h');

                    $pdf = PDF::loadView('laporan.print_status_all', compact('banjar_kelodan', 'banjar_padang', 'banjar_teguan', 'banjar_tengah', 'banjar_trinadi', 'banjar_bengawan', 'banjar_lainnya', 'count_kelodan', 'count_padang', 'count_teguan', 'count_tengah', 'count_trinadi', 'count_bengawan', 'count_lainnya', 'count_all'), [], [
                        'title'             => 'Laporan Data Status Desa Punggul',
                        'watermark'         => 'D A T A - S T A T U S',
                        'orientation'       => 'L',
                        'show_watermark'    => true
                    ]);
        
                    return $pdf->stream('Laporan-Data-Status-Desa-Punggul.pdf');
                }
                elseif($filter_status == 'all' && $filter_banjar != '')
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_status', compact('filter_banjar', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data Status '.$filter_banjar.' Desa Punggul',
                                'watermark'         => 'D A T A - S T A T U S',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-Status-'.$filter_banjar.'-Desa-Punggul.pdf');
                }
                elseif($filter_status != '' && $filter_banjar == 'all')
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_status', compact('filter_status', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data Status '.$filter_status.' Desa Punggul',
                                'watermark'         => 'D A T A - S T A T U S',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-Status-'.$filter_status.'-Desa-Punggul.pdf');
                }
                else
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_status', compact('filter_banjar', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data Status '.$filter_banjar.' Desa Punggul',
                                'watermark'         => 'D A T A - S T A T U S',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-Status-'.$filter_banjar.'-Desa-Punggul.pdf');
                }
            }
            elseif($filter == 3)
            {
                if($filter_rtrw == 'all' && $filter_banjar == 'all')
                {
                    $banjar_kelodan   = TTanahPunggul::select('banjar', 'rtrw', DB::raw('COUNT(rtrw) as br_kelodan'))->where('banjar', 'BR KELODAN')->groupBy('rtrw', 'banjar')->get();
                    $banjar_padang    = TTanahPunggul::select('banjar', 'rtrw', DB::raw('COUNT(rtrw) as br_padang'))->where('banjar', 'BR PADANG')->groupBy('rtrw', 'banjar')->get();
                    $banjar_teguan    = TTanahPunggul::select('banjar', 'rtrw', DB::raw('COUNT(rtrw) as br_teguan'))->where('banjar', 'BR TEGUAN')->groupBy('rtrw', 'banjar')->get();
                    $banjar_tengah    = TTanahPunggul::select('banjar', 'rtrw', DB::raw('COUNT(rtrw) as br_tengah'))->where('banjar', 'BR TENGAH')->groupBy('rtrw', 'banjar')->get();
                    $banjar_trinadi   = TTanahPunggul::select('banjar', 'rtrw', DB::raw('COUNT(rtrw) as br_trinadi'))->where('banjar', 'BR TRINADI')->groupBy('rtrw', 'banjar')->get();
                    $banjar_bengawan  = TTanahPunggul::select('banjar', 'rtrw', DB::raw('COUNT(rtrw) as br_bengawan'))->where('banjar', 'BR. BENGAWAN')->groupBy('rtrw', 'banjar')->get();
                    $banjar_lainnya   = TTanahPunggul::select('banjar', 'rtrw', DB::raw('COUNT(rtrw) as br_lainnya'))->where('banjar', 'DILUAR BATAS DESA')->groupBy('rtrw', 'banjar')->get();

                    $count_kelodan  = DB::select('select sum(jumlah_rtrw) as count_kelodan from (select count(rtrw) as jumlah_rtrw from t_tanah_desa_punggul where banjar = "BR KELODAN") as a');
                    $count_padang   = DB::select('select sum(jumlah_rtrw) as count_padang from (select count(rtrw) as jumlah_rtrw from t_tanah_desa_punggul where banjar = "BR PADANG") as b');
                    $count_teguan   = DB::select('select sum(jumlah_rtrw) as count_teguan from (select count(rtrw) as jumlah_rtrw from t_tanah_desa_punggul where banjar = "BR TEGUAN") as c');
                    $count_tengah   = DB::select('select sum(jumlah_rtrw) as count_tengah from (select count(rtrw) as jumlah_rtrw from t_tanah_desa_punggul where banjar = "BR TENGAH") as d');
                    $count_trinadi  = DB::select('select sum(jumlah_rtrw) as count_trinadi from (select count(rtrw) as jumlah_rtrw from t_tanah_desa_punggul where banjar = "BR TRINADI") as e');
                    $count_bengawan = DB::select('select sum(jumlah_rtrw) as count_bengawan from (select count(rtrw) as jumlah_rtrw from t_tanah_desa_punggul where banjar = "BR. BENGAWAN") as f');
                    $count_lainnya  = DB::select('select sum(jumlah_rtrw) as count_lainnya from (select count(rtrw) as jumlah_rtrw from t_tanah_desa_punggul where banjar = "DILUAR BATAS DESA") as g');
                    $count_all      = DB::select('select sum(jumlah_rtrw) as count_all from (select count(rtrw) as jumlah_rtrw from t_tanah_desa_punggul) as h');

                    $pdf = PDF::loadView('laporan.print_rtrw_all', compact('banjar_kelodan', 'banjar_padang', 'banjar_teguan', 'banjar_tengah', 'banjar_trinadi', 'banjar_bengawan', 'banjar_lainnya', 'count_kelodan', 'count_padang', 'count_teguan', 'count_tengah', 'count_trinadi', 'count_bengawan', 'count_lainnya', 'count_all'), [], [
                        'title'             => 'Laporan Data Rt/Rw Desa Punggul',
                        'watermark'         => 'D A T A - R T / R W',
                        'orientation'       => 'L',
                        'show_watermark'    => true
                    ]);
        
                    return $pdf->stream('Laporan-Data-Rt/Rw-Desa-Punggul.pdf');
                }
                elseif($filter_rtrw == 'all' && $filter_banjar != '')
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_rtrw', compact('filter_banjar', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data Rt/Rw '.$filter_banjar.' Desa Punggul',
                                'watermark'         => 'D A T A - R T / R W',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-Rt/Rw-'.$filter_banjar.'-Desa-Punggul.pdf');
                }
                elseif($filter_rtrw != '' && $filter_banjar == 'all')
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_rtrw', compact('filter_rtrw', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data Rt/Rw '.$filter_rtrw.' Desa Punggul',
                                'watermark'         => 'D A T A - R T / R W',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-Rt/Rw-'.$filter_rtrw.'-Desa-Punggul.pdf');
                }
                else
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_rtrw', compact('filter_banjar', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data Rt/Rw '.$filter_banjar.' Desa Punggul',
                                'watermark'         => 'D A T A - R T / R W',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-Rt/Rw-'.$filter_banjar.'-Desa-Punggul.pdf');
                }
            }
            elseif($filter == 4)
            {
                if($filter_znt == 'all' && $filter_banjar == 'all')
                {
                    $banjar_kelodan   = TTanahPunggul::select('banjar', 'nilai_znt', DB::raw('COUNT(nilai_znt) as br_kelodan'))->where('banjar', 'BR KELODAN')->groupBy('nilai_znt', 'banjar')->get();
                    $banjar_padang    = TTanahPunggul::select('banjar', 'nilai_znt', DB::raw('COUNT(nilai_znt) as br_padang'))->where('banjar', 'BR PADANG')->groupBy('nilai_znt', 'banjar')->get();
                    $banjar_teguan    = TTanahPunggul::select('banjar', 'nilai_znt', DB::raw('COUNT(nilai_znt) as br_teguan'))->where('banjar', 'BR TEGUAN')->groupBy('nilai_znt', 'banjar')->get();
                    $banjar_tengah    = TTanahPunggul::select('banjar', 'nilai_znt', DB::raw('COUNT(nilai_znt) as br_tengah'))->where('banjar', 'BR TENGAH')->groupBy('nilai_znt', 'banjar')->get();
                    $banjar_trinadi   = TTanahPunggul::select('banjar', 'nilai_znt', DB::raw('COUNT(nilai_znt) as br_trinadi'))->where('banjar', 'BR TRINADI')->groupBy('nilai_znt', 'banjar')->get();
                    $banjar_bengawan  = TTanahPunggul::select('banjar', 'nilai_znt', DB::raw('COUNT(nilai_znt) as br_bengawan'))->where('banjar', 'BR. BENGAWAN')->groupBy('nilai_znt', 'banjar')->get();
                    $banjar_lainnya   = TTanahPunggul::select('banjar', 'nilai_znt', DB::raw('COUNT(nilai_znt) as br_lainnya'))->where('banjar', 'DILUAR BATAS DESA')->groupBy('nilai_znt', 'banjar')->get();

                    $count_kelodan  = DB::select('select sum(jumlah_znt) as count_kelodan from (select count(nilai_znt) as jumlah_znt from t_tanah_desa_punggul where banjar = "BR KELODAN") as a');
                    $count_padang   = DB::select('select sum(jumlah_znt) as count_padang from (select count(nilai_znt) as jumlah_znt from t_tanah_desa_punggul where banjar = "BR PADANG") as b');
                    $count_teguan   = DB::select('select sum(jumlah_znt) as count_teguan from (select count(nilai_znt) as jumlah_znt from t_tanah_desa_punggul where banjar = "BR TEGUAN") as c');
                    $count_tengah   = DB::select('select sum(jumlah_znt) as count_tengah from (select count(nilai_znt) as jumlah_znt from t_tanah_desa_punggul where banjar = "BR TENGAH") as d');
                    $count_trinadi  = DB::select('select sum(jumlah_znt) as count_trinadi from (select count(nilai_znt) as jumlah_znt from t_tanah_desa_punggul where banjar = "BR TRINADI") as e');
                    $count_bengawan = DB::select('select sum(jumlah_znt) as count_bengawan from (select count(nilai_znt) as jumlah_znt from t_tanah_desa_punggul where banjar = "BR. BENGAWAN") as f');
                    $count_lainnya  = DB::select('select sum(jumlah_znt) as count_lainnya from (select count(nilai_znt) as jumlah_znt from t_tanah_desa_punggul where banjar = "DILUAR BATAS DESA") as g');
                    $count_all      = DB::select('select sum(jumlah_znt) as count_all from (select count(nilai_znt) as jumlah_znt from t_tanah_desa_punggul) as h');

                    $pdf = PDF::loadView('laporan.print_znt_all', compact('banjar_kelodan', 'banjar_padang', 'banjar_teguan', 'banjar_tengah', 'banjar_trinadi', 'banjar_bengawan', 'banjar_lainnya', 'count_kelodan', 'count_padang', 'count_teguan', 'count_tengah', 'count_trinadi', 'count_bengawan', 'count_lainnya', 'count_all'), [], [
                        'title'             => 'Laporan Data Nilai ZNT Desa Punggul',
                        'watermark'         => 'D A T A - N I L A I Z N T',
                        'orientation'       => 'L',
                        'show_watermark'    => true
                    ]);
        
                    return $pdf->stream('Laporan-Data-Nilai-ZNT-Desa-Punggul.pdf');
                }
                elseif($filter_znt == 'all' && $filter_banjar != '')
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_znt', compact('filter_banjar', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data Nilai ZNT '.$filter_banjar.' Desa Punggul',
                                'watermark'         => 'D A T A - N I L A I Z N T',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-Nilai-ZNT-'.$filter_banjar.'-Desa-Punggul.pdf');
                }
                elseif($filter_znt != '' && $filter_banjar == 'all')
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_znt', compact('filter_znt', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data Nilai ZNT Desa Punggul',
                                'watermark'         => 'D A T A - N I L A I Z N T',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-Nilai-ZNT-Desa-Punggul.pdf');
                }
                else
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = count($reports);

                    $pdf = PDF::loadView('laporan.print_znt', compact('filter_banjar', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data Nilai ZNT '.$filter_banjar.' Desa Punggul',
                                'watermark'         => 'D A T A - N I L A I Z N T',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-NILAI-ZNT-'.$filter_banjar.'-Desa-Punggul.pdf');
                }
            }
            elseif($filter == 5)
            {
                if($filter_banjar == 'all')
                {
                    $banjar_kelodan   = TTanahPunggul::select('banjar', DB::raw('SUM(njop) as br_kelodan'))->where('banjar', 'BR KELODAN')->groupBy('banjar')->first();
                    $banjar_padang    = TTanahPunggul::select('banjar', DB::raw('SUM(njop) as br_padang'))->where('banjar', 'BR PADANG')->groupBy('banjar')->first();
                    $banjar_teguan    = TTanahPunggul::select('banjar', DB::raw('SUM(njop) as br_teguan'))->where('banjar', 'BR TEGUAN')->groupBy('banjar')->first();
                    $banjar_tengah    = TTanahPunggul::select('banjar', DB::raw('SUM(njop) as br_tengah'))->where('banjar', 'BR TENGAH')->groupBy('banjar')->first();
                    $banjar_trinadi   = TTanahPunggul::select('banjar', DB::raw('SUM(njop) as br_trinadi'))->where('banjar', 'BR TRINADI')->groupBy('banjar')->first();
                    $banjar_bengawan  = TTanahPunggul::select('banjar', DB::raw('SUM(njop) as br_bengawan'))->where('banjar', 'BR. BENGAWAN')->groupBy('banjar')->first();
                    $banjar_lainnya   = TTanahPunggul::select('banjar', DB::raw('SUM(njop) as br_lainnya'))->where('banjar', 'DILUAR BATAS DESA')->groupBy('banjar')->first();

                    $pdf = PDF::loadView('laporan.print_njop_all', compact('banjar_kelodan', 'banjar_padang', 'banjar_teguan', 'banjar_tengah', 'banjar_trinadi', 'banjar_bengawan', 'banjar_lainnya'), [], [
                        'title'             => 'Laporan NJOP Banjar Desa Punggul',
                        'watermark'         => 'D A T A - N J O P',
                        'orientation'       => 'L',
                        'show_watermark'    => true
                    ]);
        
                    return $pdf->stream('Laporan-Data-NJOP-Desa-Punggul.pdf');
                }
                else
                {
                    $reports = TTanahPunggul::SearchByFilterLaporan($filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)->get();
                    $report_count = TTanahPunggul::where('banjar', $filter_banjar)->select(DB::raw('SUM(njop)as jumlah_total_njop'))->first();

                    $pdf = PDF::loadView('laporan.print_njop', compact('filter_banjar', 'reports', 'report_count'), [], [
                                'title'             => 'Laporan Data '.$filter_banjar.' Desa Punggul',
                                'watermark'         => 'D A T A - B A N J A R',
                                'orientation'       => 'L',
                                'show_watermark'    => true
                            ]);
                
                    return $pdf->stream('Laporan-Data-NJOP-'.$filter_banjar.'-Desa-Punggul.pdf');
                }
            }
        }
    }
}