<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Facades\Admin;
use App\TTanahPunggul;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        return Admin::content(function(Content $content) {
            $content->header('Dashboard');

            $total_data = TTanahPunggul::count();
            /* Title Content */
            $content->row(view('dashboard.title')->with(compact('total_data')));

            $status = TTanahPunggul::select(
                        'status as status_tanah',
                        DB::raw('count(*) as jumlah_status')    
                    )
                    ->groupBy('status')
                    ->get();
            $banjar = TTanahPunggul::select(
                        'banjar',
                        DB::raw('count(*) as jumlah_banjar')
                    )
                    ->groupBy('banjar')
                    ->get();
            /* Body Content */
            $content->row(view('dashboard.body')->with(compact('status', 'banjar')));
        });
    }
}