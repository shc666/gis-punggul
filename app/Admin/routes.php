<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('data-tanah', TanahDesaController::class)->except([
        'create', 'show' ,'store'
    ]);
    $router->get('laporan', 'LaporanController@index')->name('index.laporan');
    $router->post('laporan', 'LaporanController@index')->name('post.laporan');
    $router->post('laporan/cetak-pdf', 'LaporanController@cetakPdf')->name('cetak.laporan');
});