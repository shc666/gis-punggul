<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class TTanahPunggul extends Model
{
    use SpatialTrait;
    
    protected $table = "t_tanah_desa_punggul";
    protected $primaryKey = 'OGR_FID';
    protected $spatialFields = ['SHAPE'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'OGR_FID',
        'SHAPE', 
        'kabupaten', 
        'kecamatan',
        'desa',
        'banjar',
        'nib',
        'su',
        'luas_shat',
        'nik',
        'status',
        'nop',
        'nama_wp',
        'alamat_wp',
        'alamat_op',
        'luas_sppt',
        'nilai_znt',
        'rtrw',
        'njop',
        'bpajb',
    ];

    public function scopeSearchByFilter($query, $filter_status, $filter_banjar, $filter_nama_nik_nop_nib, $filter_njop, $filter_rtrw, $filter_znt)
    {
        if($filter_status != '')
        {
            $query->where(function($query) use($filter_status)
            {
                $query->where('status', $filter_status);                     
            });
        }
        elseif($filter_banjar != '')
        {
            $query->where(function($query) use($filter_banjar)
            {
                $query->where('banjar', $filter_banjar);                     
            });
        }
        elseif($filter_nama_nik_nop_nib != '')
        {
            $query->where(function($query) use($filter_nama_nik_nop_nib)
            {
                $query->where("nama_wp", "LIKE", "%$filter_nama_nik_nop_nib%")
                    ->orWhere("nik", "LIKE", "%$filter_nama_nik_nop_nib%")
                    ->orWhere("nib", "LIKE", "%$filter_nama_nik_nop_nib%")
                    ->orWhere("nop", "LIKE", "%$filter_nama_nik_nop_nib%");
            });
        }
        elseif($filter_njop != '')
        {
            $query->where(function($query) use($filter_njop)
            {
                $query->where('njop', $filter_njop);                     
            });
        }
        elseif($filter_rtrw != '')
        {
            $query->where(function($query) use($filter_rtrw)
            {
                $query->where('rtrw', $filter_rtrw);                     
            });
        }
        elseif($filter_znt != '')
        {
            $query->where(function($query) use($filter_znt)
            {
                $query->where('nilai_znt', $filter_znt);                     
            });
        }
    }

    public function scopeSearchByFilterLaporan($query, $filter, $filter_banjar, $filter_status, $filter_rtrw, $filter_znt)
    {
        if($filter == 1)
        {
            if($filter_banjar == 'all')
            {
                return $query;
            }
            else
            {
                $query->where(function($query) use($filter_banjar)
                {
                    $query->where('banjar', $filter_banjar);                     
                });
            }
        }
        elseif($filter == 2)
        {
            if($filter_status == 'all' && $filter_banjar == 'all')
            {
                return $query;
            }
            elseif($filter_status == 'all' && $filter_banjar != '')
            {
                $query->where(function($query) use($filter_banjar)
                {
                    $query->where('banjar', $filter_banjar);                    
                });
            }
            elseif($filter_status != '' && $filter_banjar == 'all')
            {
                $query->where(function($query) use($filter_status)
                {
                    $query->where('status', $filter_status);                   
                });
            }
            else
            {
                $query->where(function($query) use($filter_status, $filter_banjar)
                {
                    $query->where([
                        'banjar' => $filter_banjar,
                        'status' => $filter_status
                    ]);                     
                });
            }
        }
        elseif($filter == 3)
        {
            if($filter_rtrw == 'all' && $filter_banjar == 'all')
            {
                return $query;
            }
            elseif($filter_rtrw == 'all' && $filter_banjar != '')
            {
                $query->where(function($query) use($filter_banjar)
                {
                    $query->where('banjar', $filter_banjar);                    
                });
            }
            elseif($filter_rtrw != '' && $filter_banjar == 'all')
            {
                $query->where(function($query) use($filter_rtrw)
                {
                    $query->where('rtrw', $filter_rtrw);                   
                });
            }
            else
            {
                $query->where(function($query) use($filter_rtrw, $filter_banjar)
                {
                    $query->where([
                        'banjar' => $filter_banjar,
                        'rtrw'   => $filter_rtrw
                    ]);                     
                });
            }
        }
        elseif($filter == 4)
        {
            if($filter_znt == 'all' && $filter_banjar == 'all')
            {
                return $query;
            }
            elseif($filter_znt == 'all' && $filter_banjar != '')
            {
                $query->where(function($query) use($filter_banjar)
                {
                    $query->where('banjar', $filter_banjar);                    
                });
            }
            elseif($filter_znt != '' && $filter_banjar == 'all')
            {
                $query->where(function($query) use($filter_znt)
                {
                    $query->where('nilai_znt', $filter_znt);                   
                });
            }
            else
            {
                $query->where(function($query) use($filter_znt, $filter_banjar)
                {
                    $query->where([
                        'banjar'    => $filter_banjar,
                        'nilai_znt' => $filter_znt
                    ]);                     
                });
            }
        }
        elseif($filter == 5)
        {
            if($filter_banjar == 'all')
            {
                return $query;
            }
            else
            {
                $query->where(function($query) use($filter_banjar)
                {
                    $query->where('banjar', $filter_banjar);                     
                });
            }
        }
    }
}