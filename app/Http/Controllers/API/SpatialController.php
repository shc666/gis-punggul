<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Http\Resources\Outlet as OutletResource;
use App\TTanahPunggul;
use DB;

class SpatialController extends Controller
{
    /**
     * Get outlet listing on Leaflet JS geoJSON data structure.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        // $outlets = TTanahPunggul::all();
        $outlets = TTanahPunggul::select(
                    '*',
                    DB::raw("
                        (CASE 
                            WHEN status = 'Sudah Terdaftar' THEN '#0056F5'
                            WHEN status = 'Sudah Teridentifikasi' THEN '#7EF500'
                            ELSE '#F50000'
                        END) as color_font
                    ")
                    )->get();

        $outlets_status = TTanahPunggul::select(
                            'status',
                            DB::raw('COUNT(*) as jumlah_status')
                        )
                        ->groupBy('status')
                        ->orderBy('jumlah_status', 'desc')
                        ->get();

        $outlets_banjar = TTanahPunggul::select(
                            'banjar',
                            DB::raw('COUNT(*) as jumlah_banjar')
                        )
                        ->groupBy('banjar')
                        ->orderBy('jumlah_banjar', 'desc')
                        ->get();

        $outlets_rtrw = TTanahPunggul::select(
                            'rtrw',
                            DB::raw('COUNT(*) as jumlah_rtrw')
                        )
                        ->groupBy('rtrw')
                        ->get();

        $outlets_znt = TTanahPunggul::select(
                            'nilai_znt',
                            DB::raw('COUNT(*) as jumlah_znt')                
                        )
                        ->groupBy('nilai_znt')
                        ->get();

        $outlets_count = TTanahPunggul::count();

        $geoJSONdata = $outlets->map(function ($outlet) {
            return [
                'type'       => 'Feature',
                'geometry'   => $outlet->SHAPE,
                'properties' => new OutletResource($outlet)
            ];
        });

        return response()->json([
            'count_data'    => $outlets_count,
            'data_status'   => $outlets_status,
            'data_banjar'   => $outlets_banjar,
            'data_znt'      => $outlets_znt,
            'data_rtrw'     => $outlets_rtrw,
            'type'          => 'FeatureCollection',
            'features'      => $geoJSONdata
        ]);
    }

    /**
     * Get outlet filter post on Leaflet JS geoJSON data structure.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Illuminate\Http\JsonResponse
     */
    public function postFilter()
    {
        $filter_status = Input::get('status', null);
        $filter_banjar = Input::get('banjar', null);
        $filter_znt = Input::get('znt', null);
        $filter_rtrw = Input::get('rtrw', null);
        $filter_nama_nik_nop_nib = Input::get('nama_nik_nop_nib', null);
        $filter_njop = Input::get('njop', null);

        $outlets = TTanahPunggul::SearchByFilter($filter_status, $filter_banjar, $filter_nama_nik_nop_nib, $filter_njop, $filter_rtrw, $filter_znt)
                    ->select(
                        '*',
                        DB::raw("
                            (CASE 
                                WHEN status = 'Sudah Terdaftar' THEN '#0056F5'
                                WHEN status = 'Sudah Teridentifikasi' THEN '#7EF500'
                                ELSE '#F50000'
                            END) as color_font
                        ")
                        )
                    ->get();
        $total_outlets = count($outlets);

        $geoJSONdata = $outlets->map(function ($outlet) {
            return [
                'type'       => 'Feature',
                'geometry'   => $outlet->SHAPE,
                'properties' => new OutletResource($outlet)
            ];
        });

        return response()->json([
            'count_data'    => $total_outlets,
            'type'          => 'FeatureCollection',
            'features'      => $geoJSONdata
        ]);
    }
}