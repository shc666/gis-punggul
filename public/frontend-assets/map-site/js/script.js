function openNav() {
    document.getElementById("rightSideNav").style.width = "350px";
    document.getElementById("toggle-btn-right").style.display = "none";
}

function closeNav() {
    document.getElementById("rightSideNav").style.width = "0";
    document.getElementById("toggle-btn-right").style.display = "block";
}

$(window).on("load", function () {
    "use strict";

    /* ===================================
            Loading Timeout
     ====================================== */

    $('.side-menu').removeClass('hidden');
    setTimeout(function(){
        $("#loader").fadeOut(700);
    }, 1000);
});

jQuery(function ($) {
    "use strict";
    
    /* ===================================
        Side Menu
    ====================================== */
    if ($(".toggle-btn").length) {
        $(".toggle-btn").on("click", function () {
            $(".pushwrap").toggleClass("active");
            $(".side-menu").addClass("side-menu-active"), $("#close_side_menu").fadeIn(700)
        }), $("#close_side_menu").on("click", function () {
            $(".side-menu").removeClass("side-menu-active"), $(this).fadeOut(200), $(".pushwrap").removeClass("active")
        }), $(".side-nav-menu .navbar-nav .nav-link").on("click", function () {
            $(".side-menu").removeClass("side-menu-active"), $("#close_side_menu").fadeOut(200), $(".pushwrap").removeClass("active")
        }), $("#btn_sideNavClose").on("click", function () {
            $(".side-menu").removeClass("side-menu-active"), $("#close_side_menu").fadeOut(200), $(".pushwrap").removeClass("active")
        });
    }

    /* =====================================
         Side modal close
    ====================================== */
    $('.content-area').on('click',function () {
        $(".side-menu").removeClass("side-menu-active");
        // $(this).fadeOut(200);
        $(".pushwrap").removeClass("active");
    });

    /* =====================================
         Parallax
    ====================================== */
    if($(window).width() < 780) {
        $('.parallax').addClass("parallax-disable");
    } else {
        $('.parallax').removeClass("parallax-disable");

        // parallax
        $(".parallax").parallaxie({
            speed: 0.55,
            offset: -100,
        });
    }
    
    /* =====================================
            Header appear
    ====================================== */
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 260) { // Set position from top to add class
            $('.upper-nav').addClass('header-appear');
        }
        else {
            $('.upper-nav').removeClass('header-appear');
        }
    });

    /* ===================================
            Clearable text inputs
    ====================================== */
    $(".clearable").each(function() {
        var $input = $(this).find("input:text"),
            $clear = $(this).find(".clearable__clear");

        $input.on("input", function(){
            $clear.toggle(!!this.value);
        });
        
        $clear.on("touchstart click", function(e) {
            e.preventDefault();
            $input.val("").trigger("input");

            window.location.reload();
        });
    });
});